# frozen_string_literal: true

control 'bonded-ethernet-config-file-netbond-config-managed' do
  title 'should match desired lines'

  describe file('/etc/netplan/02-netbond.yaml') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('network:') }
  end
end
