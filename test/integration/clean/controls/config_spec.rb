# frozen_string_literal: true

control 'bonded-ethernet-config-file-netbond-config-absent' do
  title 'should be absent'

  describe file('/etc/netplan/02-netbond.yaml') do
    it { should_not exist }
  end
end
