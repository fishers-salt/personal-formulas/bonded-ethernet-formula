# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as bonded__ethernet with context %}

bonded-ethernet-config-file-netbond-config-absent:
  file.absent:
    - name: /etc/netplan/02-netbond.yaml
